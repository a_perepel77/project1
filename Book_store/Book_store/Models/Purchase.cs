﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Book_store.Models
{
    public class Purchase
    {
        //ID pokupki
        public int PurchaseId { get; set; }
        //FIO pokupatelya
        public string Person { get; set; }
        //adres pokupatelya
        public string Address { get; set; }
        //ID knigi
        public int BookId { get; set; }
        //data pokupki
        public DateTime Date { get; set; }
    }
}